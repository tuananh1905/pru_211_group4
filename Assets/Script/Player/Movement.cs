using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float speed, jumpHigh;
    private float moveX;
    private float moveY;
    private Rigidbody2D rb;
    private Animator anim;
    private bool isfacingRight = true;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            rb.velocity = new Vector2(rb.velocity.x, moveY+jumpHigh);
        }
        moveX = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveX * speed, rb.velocity.y);
        flip();
        anim.SetFloat("Move", Mathf.Abs(moveX));
        anim.SetBool("Jump", Input.GetButtonDown("Jump"));
    }

    void flip()
    {
        if (isfacingRight && moveX < 0 || !isfacingRight && moveX > 0)
        {
            isfacingRight = !isfacingRight;
            Vector3 scale = transform.localScale;
            scale.x = scale.x * -1;
            transform.localScale = scale;
        }
    }
}
